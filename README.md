#pixel NN performace with less charge info

ToT of FE-I4 : 4 bits
ToT of FE-I3 : 8 bits

#converted MC data to 4 bits

##source: 

###training
"/afs/cern.ch/work/l/lgagnon/public/data/NNinput/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0.number.training.root"
###test
"/afs/cern.ch/work/l/lgagnon/public/data/NNinput/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0.number.test.root"


##4bits:

###training
"/afs/cern.ch/work/s/sosen/public/MC/4bits/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0_4bits.number.training.root"

###test
"/afs/cern.ch/work/s/sosen/public/MC/4bits/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0_4bits.number.test.root"


##8bits:

###training
"/afs/cern.ch/work/s/sosen/public/MC/4bits/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0_8bits.number.training.root"

##test
"/afs/cern.ch/work/s/sosen/public/MC/4bits/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0_8bits.number.test.root"



#Ongoing task


  [*] Submitted training of the 4bit data: /afs/cern.ch/work/s/sosen/public/MC/4bits/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0_4bits.number.training.root

    [sosen@lxplus0074 pixel-NN-training]$ nohup python trainNN_keras.py --training-input $INPUT --output 4bit_train_11_7_2016 --config <(python genconfig.py --type number) &
    [2] 15775

  [*] Submitted training of the 4bit data: /afs/cern.ch/work/s/sosen/public/MC/4bits/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0_4bits.number.training.root
    [sosen@lxplus0074 reduced_charge_pixel_NN_performance]$ nohup python trainNN_keras.py --training-input $INPUT --output 8bit_train_11_7_2016 --config <(python genconfig.py --type number) &
    [1] 29236

  [*] Submitted training of the MC data: /afs/cern.ch/work/l/lgagnon/public/data/NNinput/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0.number.training.root

    [sosen@lxplus090 pixel-NN-training]$ nohup python trainNN_keras.py --training-input $INPUT --output MC_train_11_7_2016 --config <(python genconfig.py --type number) &
    [1] 30244


   [*] Submitted test of the MC data: /afs/cern.ch/work/l/lgagnon/public/data/NNinput/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0.number.test.root
    [sosen@lxplus0074 pixel-NN-training]$ nohup python evalNN_keras.py --input $TEST_INPUT --model MC_train_11_7_2016.model.yaml --weights MC_train_11_7_2016.weights.hdf5  --normalization MC_train_11_7_2016.normalization.txt --output MC_train_11_7_2016.db --config <(python genconfig.py --type number) &
    [1] 19188

   [*] Submitted test of the 4bit data: /afs/cern.ch/work/s/sosen/public/MC/4bits/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0_4bits.number.test.root

     [sosen@lxplus014 pixel-NN-training]$ nohup python evalNN_keras.py --input $TEST_INPUT --model 4bit_train_11_7_2016.model.yaml --weights 4bit_train_11_7_2016.weights.hdf5  --normalization 4bit_train_11_7_2016.normalization.txt --output 4bit_train_11_7_2016.db --config <(python genconfig.py --type number) &
     [1] 10133



  [*] Converting MC training to 8bits using MC training data: /afs/cern.ch/work/l/lgagnon/public/data/NNinput/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0.number.training.root
      to "/afs/cern.ch/work/s/sosen/public/MC/4bits/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0_8bits.number.training.root"
    [sosen@lxplus0074 reduced_charge_pixel_NN_performance]$ nohup sh submit_convert.sh &
    [1] 10320





  [*] Converted MC training to 8bits using MC test data: /afs/cern.ch/work/l/lgagnon/public/data/NNinput/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0.number.test.root
      to "/afs/cern.ch/work/s/sosen/public/MC/4bits/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0_8bits.number.test.root"

     
  [ ] To store max-min of ToT charge for each pixel from training and testing data set.
    
