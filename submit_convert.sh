export bit_res=8; #8bit storeage
export fin="/afs/cern.ch/work/l/lgagnon/public/data/NNinput/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0.number.test.root";
export fout="/afs/cern.ch/work/s/sosen/public/MC/4bits/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0_${bit_res}bits.number.test.root";
root -l "TOTNbit_converter.C(\"${fin}\", \"${fout}\", ${bit_res})" ;
