import ROOT
import subprocess
def plot(p,n,layer):
   f1 = ROOT.TFile("../pixel-NN-training/4bit_train_11_7_2016.root")
   f2 = ROOT.TFile("../pixel-NN-training/8bitbig.root")
   hist="ROC_%ivs%i_%s_all"%(p,n,layer)
   c = ROOT.TCanvas("c", "c", 0, 0, 800, 600)
   g1 = f1.Get(hist)
   g1.SetTitle(';False positive rate;False Negative Rate;')
   g1.SetLineColor(ROOT.kBlue)
   g1.SetMarkerSize(0.05)
   g1.Draw()
   g2 = f2.Get(hist)
   g2.SetLineColor(ROOT.kRed)
   g2.SetMarkerSize(0.05)
   g2.Draw("same")
   out=hist+".eps"
   c.SaveAs(out)
   subprocess.call(['ps2pdf', '-dEPSCrop', out])
   subprocess.call(['rm', out])
 
def main():
   layers = ['all', 'barrel', 'endcap', 'ibl']
   for p in range(1,4):
     for n in range(1,4):
       for layer in layers:
         if(p!=n): plot(p,n,layer)
 
if __name__ == '__main__':
    main()
