import argparse

parser = argparse.ArgumentParser(description='incident angle plots with filter')
parser.add_argument('--train_data', action='store_true')
parser.add_argument('--angle', required=True, choices=['theta','phi'])
parser.add_argument('--layer', default='all_barrel', choices=['split_barrel', 'all_barrel'])
parser.add_argument('--particle', default='all', choices=['1', '2', '3', 'all'])
par=parser.parse_args()


import ROOT
import numpy as np
import root_numpy as rnp

from rootpy.plotting import Canvas

from math import pi


testing_set = "/afs/cern.ch/work/l/lgagnon/public/data/NNinput/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0.number.test.root"
training_set = "/afs/cern.ch/work/l/lgagnon/public/data/NNinput/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0.number.training.root"
path = testing_set
if (par.train_data): 
  path = training_set
  file_name = 'train'
  print "using: ", training_set
else:
  file_name = 'test'
  print "using: ", testing_set


f = ROOT.TFile(path, 'READ')
t = f.Get('NNinput')
n = t.GetEntries()

if(par.particle == '1'): particle = rnp.tree2array(t, "NN_nparticles1")
elif(par.particle == '2'): particle = rnp.tree2array(t, "NN_nparticles2")
elif(par.particle == '3'): particle = rnp.tree2array(t, "NN_nparticles3")
else: particle = np.array([1]*n)

if (par.angle == 'phi'): inc_angle = rnp.tree2array(t, "NN_phi")
else: inc_angle = rnp.tree2array(t, "NN_theta")
barrelEC = rnp.tree2array(t, "NN_barrelEC")
etaModule = rnp.tree2array(t, "NN_etaModule")
etamod_barrel = range(-6,7)
layerid = rnp.tree2array(t, "NN_layer")

if (par.layer == 'all_barrel'): lname = ['all_barrel']
else: lname = ['ibl', 'layer1', 'layer2', 'layer3']

h = []
for l in lname:
  h.append([])
  for etaid in etamod_barrel:
    h[-1].append(ROOT.TH1F("%s_%s_%s_%sp_eta%i"%(file_name, par.angle, l, par.particle, etaid), "%s_%s_%s_%sp_eta%i"%(file_name, par.angle, l, par.particle, etaid), 1000, -pi/2., pi/2.))


for event in range(n):
  if (particle[event] == 1):
    if (barrelEC[event] == 0):
      for ln in range(len(lname)):
        if (ln != layerid[event]): continue
	for et in range(len(etamod_barrel)):
	  if (etamod_barrel[et] == etaModule[event]): h[ln][et].Fill(inc_angle[event])



for ll in range(len(lname)):
  for et in range(len(etamod_barrel)):
    c = Canvas()
    h[ll][et].Draw('hist')
    c.Print('%s_%s_%s_%sp_%i.eps'%(file_name, par.angle, lname[ll], par.particle, etamod_barrel[et]))
