local_python
for particle in {1,2,3}
do
  mkdir ${particle}p
  cp combine.py ${particle}p
  cp incidence_cluster.py ${particle}p
  cd ${particle}p
  echo "nohup python incidence_cluster.py --angle phi --layer split_barrel --particle ${particle} &" > job.sh
  echo "nohup python incidence_cluster.py --train_data --angle phi --layer split_barrel --particle ${particle} &" >> job.sh
  echo "nohup python incidence_cluster.py --angle theta --layer split_barrel --particle ${particle} &" >> job.sh
  echo "nohup python incidence_cluster.py --train_data --angle theta --layer split_barrel --particle ${particle} &" >> job.sh
  source job.sh &
  cd ..
done
