def etaid(list_, etid_, ang_, data_):
  k = []
  for i in list_:
    if (i.find("%s") != -1): 
      add=[data_, ang_, nparticle_]
      if (i.find("%i") != -1):
        add.append(etid_)
      i=i%tuple(add)
    k.append(i)
  return k

def clusterid(list_, etid_, data_):
  m = []
  for i in list_:
    if (i.find("%s") != -1): 
      add=[data_, nparticle_]
      if (i.find("%i") != -1):
        add.append(etid_)
      i=i%tuple(add)
    m.append(i)
  return m

  

def printinfile(list_):
  f = open('plots_incidence_%sp.tex'%nparticle_, 'a')
  for l in list_:
    f.write(l)
  f.close()
  return

def main():
  global nparticle_
  nparticle_ = raw_input("# of particle(1/2/3): ")
  f = open('plots_incidence_%sp.tex'%nparticle_, 'w')
  f.close()

  start = ['\\documentclass[a4paper,12pt]{article}\n', '\\usepackage{graphicx}\n', '\\usepackage{subcaption}\n', '\\begin{document}\n']
  printinfile(start)
  etamodid = range(-6,7)
  plots = ['\n', '\\begin{figure}[htb]\n', '  \\centering\n', '  \\begin{tabular}{@{}cccc@{}}\n', '    \\includegraphics[width=.50\\textwidth]{%s_%s_ibl_%sp_%i.eps} &\n', '    \\includegraphics[width=.50\\textwidth]{%s_%s_layer1_%sp_%i.eps} \\\\\n', '    \\includegraphics[width=.50\\textwidth]{%s_%s_layer2_%sp_%i.eps} &\n', '    \\includegraphics[width=.50\\textwidth]{%s_%s_layer3_%sp_%i.eps} \\\\\n', '  \\end{tabular}\n', '  \\caption{Using %s data, $\\%s$ distribution of %s-particle clusters for $\\eta$ module %i}\n', '\\end{figure}\n', '\\clearpage\n', '\n']
  pixel_2d = ['\n', '\\begin{figure}[htb]\n', '  \\centering\n', '  \\begin{tabular}{@{}cccc@{}}\n', '    \\includegraphics[width=.50\\textwidth]{cluster_%s_%sp_ibl_etaid%i.eps} &\n', '    \\includegraphics[width=.50\\textwidth]{cluster_%s_%sp_layer1_etaid%i.eps} \\\\\n', '    \\includegraphics[width=.50\\textwidth]{cluster_%s_%sp_layer2_etaid%i.eps} &\n', '    \\includegraphics[width=.50\\textwidth]{cluster_%s_%sp_layer3_etaid%i.eps} \\\\\n', '  \\end{tabular}\n', '  \\caption{Using %s data, 2D pixel average charge of %s-particle clusters for $\\eta$ module %i}\n', '\\end{figure}\n', '\\clearpage\n', '\n']
  for var in ['phi', 'theta', 'cluster2d']:
    for i in etamodid:
      for data in ['test', 'train']:
	if (var != 'cluster2d'): printinfile(etaid(plots, i, var, data))
	else: printinfile(clusterid(pixel_2d, i, data))

  end = ['\\end{document}\n']
  printinfile(end)

if __name__ == '__main__':
     main()
