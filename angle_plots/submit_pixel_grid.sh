local_python
for particle in {1,2,3}
do
  cp pixel_grid.py ${particle}p
  cp combine.py ${particle}p
  cd ${particle}p
  echo "nohup python pixel_grid.py  --layer split_barrel --particle ${particle} &" > job_pixelq.sh
  echo "nohup python pixel_grid.py --train_data  --layer split_barrel --particle ${particle} &" >> job_pixelq.sh
  source job_pixelq.sh &
  cd ..
done
