import argparse

parser = argparse.ArgumentParser(description='charge pixel 2D distribution with filter')
parser.add_argument('--train_data', action='store_true')
parser.add_argument('--layer', default='all_barrel', choices=['split_barrel', 'all_barrel'])
parser.add_argument('--particle', default='all', choices=['1', '2', '3', 'all'])
par=parser.parse_args()

import ROOT
import numpy as np
import root_numpy as rnp
from rootpy.plotting import Canvas

testing_set = "/afs/cern.ch/work/l/lgagnon/public/data/NNinput/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0.number.test.root"
training_set = "/afs/cern.ch/work/l/lgagnon/public/data/NNinput/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0.number.training.root"
path = testing_set
if (par.train_data):
  path = training_set
  file_alias = 'train'
  print "using: ", training_set
else:
  file_alias = 'test'
  print "using: ", testing_set


f = ROOT.TFile(path, 'READ')
t = f.Get('NNinput')
n = 1#t.GetEntries()

if(par.particle == '1'): particle = rnp.tree2array(t, "NN_nparticles1")
elif(par.particle == '2'): particle = rnp.tree2array(t, "NN_nparticles2")
elif(par.particle == '3'): particle = rnp.tree2array(t, "NN_nparticles3")
else: particle = np.array([1]*n)

barrelEC = rnp.tree2array(t, "NN_barrelEC")
etaModule = rnp.tree2array(t, "NN_etaModule")
etamod_barrel = range(-6,7)
layerid = rnp.tree2array(t, "NN_layer")

if (par.layer == 'all_barrel'): lname = ['all_barrel']
else: lname = ['ibl', 'layer1', 'layer2', 'layer3']


cluster_charge = []
sizeX=7
sizeY=7
for etaid in etamod_barrel:
  cluster_charge.append([])
  for layer in lname:
    cluster_charge[-1].append([0]*49)
charge_matrix = []
for pix in range(sizeX*sizeY):
  charge_matrix.append(rnp.tree2array(t,"NN_matrix%i"%pix))


for event in range(n):
  if (particle[event] == 1):
    if (barrelEC[event] == 0):
      for ln in range(len(lname)):
        if (ln != layerid[event]): continue
        for et in range(len(etamod_barrel)):
	  if (etamod_barrel[et] == etaModule[event]):
            for pix in range(sizeX*sizeY):
              cluster_charge[et][ln][pix] += charge_matrix[pix][event]

for ll in range(len(lname)):
  for et in range(len(etamod_barrel)):
    c = Canvas()
    hist = ROOT.TH2F("cluster_%sp_%s_etaid%i"%(par.particle, lname[ll], etamod_barrel[et]), "cluster_%sp_%s_etaid%i"%(par.particle, lname[ll], etamod_barrel[et]), sizeX, -0.5, sizeX-0.5, sizeY, -0.5, sizeY-0.5)
    for pix in range(sizeX*sizeY):
      #print pix/sizeX, pix%sizeY
      hist.SetBinContent((pix/sizeX)+1, (pix%sizeY)+1, cluster_charge[et][ll][pix])
    hist.Draw('TEXT')#LEGO20')
    c.Print("cluster_%s_%sp_%s_etaid%i.eps"%(file_alias, par.particle, lname[ll], etamod_barrel[et]))
