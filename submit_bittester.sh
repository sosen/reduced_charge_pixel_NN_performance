#export fin="/afs/cern.ch/work/l/lgagnon/public/data/NNinput/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0.number.training.root"
export fin="/afs/cern.ch/work/s/sosen/public/MC/4bits/group.perf-idtracking.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.AOD_TIDE.e3668_s2608_s2183_MC16Test_v3_EXT0_4bits.number.test.root"
export nbins=100
export pixelindex=30
root -l "tester8bit.C(\"${fin}\",${nbins}, ${pixelindex})"
